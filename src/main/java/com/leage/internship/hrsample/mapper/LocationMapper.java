package com.leage.internship.hrsample.mapper;

import com.leage.internship.hrsample.model.Location;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface LocationMapper {

    @Select("SELECT * FROM locations")
    List<Location> getAll();

    @Insert("INSERT INTO locations(street_address, city)" +
            "VALUES (#{location.streetAddress}, #{location.city})")
    void save(@Param("location") Location location);
}


