package com.leage.internship.hrsample.mapper;

import com.leage.internship.hrsample.model.Department;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface DepartmentMapper {

    @Select("SELECT * FROM departments")
    List<Department> getAll();

    @Insert("INSERT INTO departments(department_name, manager_id, location_id)" +
            " VALUES (#{department.department_name}, #{department.managerId}, #{department.locationId}) ")
    void save(@Param("department") Department department);
}
