package com.leage.internship.hrsample.service.api;

import com.leage.internship.hrsample.dto.LocationSaveDTO;
import com.leage.internship.hrsample.model.Location;

import java.util.List;

public interface LocationService {

    List<Location> getAll();

    void save(LocationSaveDTO saveDTO);
}
