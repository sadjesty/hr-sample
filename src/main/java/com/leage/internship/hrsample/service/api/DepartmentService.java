package com.leage.internship.hrsample.service.api;

import com.leage.internship.hrsample.dto.DepartmentSaveDTO;
import com.leage.internship.hrsample.model.Department;

import java.util.List;

public interface DepartmentService {

    List<Department> getAll();

    void save(DepartmentSaveDTO saveDTO);
}
