package com.leage.internship.hrsample.service;

import com.leage.internship.hrsample.dto.LocationSaveDTO;
import com.leage.internship.hrsample.mapper.LocationMapper;
import com.leage.internship.hrsample.model.Location;
import com.leage.internship.hrsample.service.api.LocationService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationServiceImpl implements LocationService {

    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    private LocationMapper locationMapper;

    @Override
    public List<Location> getAll() {
        return locationMapper.getAll();
    }

    @Override
    public void save(LocationSaveDTO saveDTO) {

        Location location = mapper.map(saveDTO, Location.class);
        locationMapper.save(location);
    }

}
