package com.leage.internship.hrsample.service;

import com.leage.internship.hrsample.dto.DepartmentSaveDTO;
import com.leage.internship.hrsample.mapper.DepartmentMapper;
import com.leage.internship.hrsample.model.Department;
import com.leage.internship.hrsample.service.api.DepartmentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    private DepartmentMapper departmentMapper;

    @Override
    public List<Department> getAll() {
        return departmentMapper.getAll();
    }

    @Override
    public void save(DepartmentSaveDTO saveDTO) {

        Department department = mapper.map(saveDTO, Department.class);
        departmentMapper.save(department);
    }
}
