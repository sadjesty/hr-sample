package com.leage.internship.hrsample;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.leage.internship.hrsample.mapper")
public class HrSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(HrSampleApplication.class, args);
    }

}
