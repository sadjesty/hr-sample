package com.leage.internship.hrsample.model;

import lombok.Data;

@Data
public class Job {

    private Integer jobId;
    private String jobTitle;
    private Integer minSalary;
    private Integer maxSalary;
}
