package com.leage.internship.hrsample.model;

import lombok.Data;

@Data
public class Department {

    private Integer departmentId;
    private String departmentName;
    private Integer managerId;
    private Integer locationId;
}
