package com.leage.internship.hrsample.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class JobHistory {

    private Integer employeeId;
    private LocalDate startDate;
    private LocalDate endDate;
    private String jobId;
    private Integer departmentId;
}
