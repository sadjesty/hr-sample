package com.leage.internship.hrsample.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Employee {

    private Integer employeeId;
    private String firstName;
    private String lastName;
    private String email;
    private LocalDate hireDate;
    private String jobId;
    private Integer salary;
    private Integer managerId;
    private Integer departmentId;
}
