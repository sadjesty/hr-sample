package com.leage.internship.hrsample.model;

import lombok.Data;

@Data
public class Location {

    private Integer locationId;
    private String streetAddress;
    private String city;
}
