package com.leage.internship.hrsample.dto;

import lombok.Data;

@Data
public class LocationSaveDTO {

    private String streetAddress;
    private String city;
}
