package com.leage.internship.hrsample.dto;

public class DepartmentSaveDTO {

    private String departmentName;
    private Integer managerId;
    private Integer locationId;
}
