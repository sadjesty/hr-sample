package com.leage.internship.hrsample.controller;

import com.leage.internship.hrsample.dto.DepartmentSaveDTO;
import com.leage.internship.hrsample.model.Department;
import com.leage.internship.hrsample.service.api.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/departments")
public class DepartmentsController {

    @Autowired
    private DepartmentService departmentService;

    @GetMapping
    public List<Department> getAll() {
        return departmentService.getAll();
    }

    @RequestMapping
    public void save(@RequestBody DepartmentSaveDTO saveDTO) {
        departmentService.save(saveDTO);
    }
}
