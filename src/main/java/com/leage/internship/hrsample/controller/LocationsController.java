package com.leage.internship.hrsample.controller;

import com.leage.internship.hrsample.dto.LocationSaveDTO;
import com.leage.internship.hrsample.model.Location;
import com.leage.internship.hrsample.service.api.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/locations")
public class LocationsController {

    @Autowired
    private LocationService locationService;

    @GetMapping
    public List<Location> getAll() {

        return locationService.getAll();
    }

    @PostMapping
    public void save(@RequestBody LocationSaveDTO saveDTO) {

        locationService.save(saveDTO);
    }
}
