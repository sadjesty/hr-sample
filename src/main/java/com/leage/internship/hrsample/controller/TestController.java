package com.leage.internship.hrsample.controller;

import com.leage.internship.hrsample.model.Location;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("/test")
    public Location getLocationTest() {

        return new Location();
    }
}
